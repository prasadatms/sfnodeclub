# SFNodeClub
A simple web application built using Express/Node, Angular, Mongo, Jade templates, Stylus and few other tools

Project Requirements:

Node v4.3.0

Express v4.13.4

Mongoose v4.4.11

body-parser v1.15.0

jade v1.11.0

stylus v0.54.2
